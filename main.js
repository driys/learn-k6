import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import { login } from "./js/tm_digital/login.js";
import { createTechnology, getAllTechnology, getTechnologyByName, editTechnology, deleteTechnology } from "./js/tm_digital/md_technology.js";
import { sleep } from 'k6';
import { createTestingApps, deleteTestingApps, editTestingApps, getTestingAppByName } from "./js/tm_digital/md_testingapps.js";
import { createNewProject, deleteProject, editProject, getDetailProject, getProjectByName } from "./js/tm_digital/project_management.js";
import { setup, options } from "./js/tm_digital/support/setup.js";
import { createUser, deleteUser, getUserByName, updateUser } from "./js/tm_digital/user_management.js";
import { createNewFeature, deleteFeature, getAllFeature, updateFeature } from "./js/tm_digital/feature_management.js";

export { options };  // Exporting options so that k6 uses them
const { url } = setup();

export default function main() {
  // Login to generate token
  const email = 'superadmin@gmail.com';
  const password = 'superadmin';

  const token = login(url, email, password);

  // User Management
  // let nameUser = createUser(url, token);
  // let idUser = getUserByName(url, token, nameUser);
  // try {
  //   updateUser(url, token, idUser, nameUser);
  // } catch (error) {
  //   console.log('Error - Failed to Update ' + nameUser);
  // }
  // try {
  //   deleteUser(url, token, idUser, nameUser)
  // } catch (error) {
  //   console.log('Error - Failed to Delete ' + nameUser);
  // }

  // Project
  let projectName = createNewProject(url, token);
  let projectId = getProjectByName(url, token, projectName);
  try {
    getDetailProject(url, token, projectId);
  } catch (error) {
    console.log('Error - Failed to Get Detail ' + projectName);
  }
  try {
    editProject(url, token, projectId);
  } catch (error) {
    console.log('Error - Failed to Edit ' + projectName);
  }
  try {
    deleteProject(url, token, projectId);
  } catch (error) {
    console.log('Error - Failed to Delete ' + projectName);
  }

  // Feature --> Masih Error
  // let nameFeature, project_id = createNewFeature(url, token);
  // let id = getAllFeature(url, token, project_id);
  // try {
  //   updateFeature(url, token, id);
  // } catch (error) {
  //   console.log('Error - Failed to Update ' + nameFeature);
  // }
  // try {
  //   deleteFeature(url, token, id)
  // } catch (error) {
  //   console.log('Error - Failed to Delete ' + nameFeature);
  // }

  // Master Data - Technology
  // let technologyName = createTechnology(url, token);
  // let technologyId = getTechnologyByName(url, token, technologyName);
  // try {
  //   editTechnology(url, token, technologyId);
  // } catch (error) {
  //   console.log('Error - Failed to Edit ' + technologyName);
  // }
  // try {
  //   deleteTechnology(url, token, technologyId);
  // } catch (error) {
  //   console.log('Error - Failed to Delete ' + technologyName);
  // }

  // Master Data - Testing Application
  // let testAppName = createTestingApps(url, token);
  // let testAppId = getTestingAppByName(url, token, testAppName, testAppId);
  // try {
  //   editTestingApps(url, token, testAppId);
  // } catch (error) {
  //   console.log('Error - Failed to Edit ' + testAppName);
  // }
  // try {
  //   deleteTestingApps(url, token, testAppId);
  // } catch (error) {
  //   console.log('Error - Failed to Delete ' + testAppName);
  // }

  sleep(Math.random() * 2 + 1);
}

export function handleSummary(data) {
  return {
    "report/summary.html": htmlReport(data),
  };
}