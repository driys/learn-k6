const { exec } = require('child_process');
const path = require('path');

function runK6Script(scriptPath, env) {
    // Platform-specific environment variable setting
    const isWindows = process.platform === 'win32';
    const setDashboardEnv = isWindows ? 'set K6_WEB_DASHBOARD=true&&' : 'export K6_WEB_DASHBOARD=true&&';
    
    // Construct the full command string
    const k6Command = `${setDashboardEnv} k6 run -e TEST_ENV=${env} ${scriptPath}`;

    exec(k6Command, (error, stdout, stderr) => {
        if (error) {
            console.error(`Error executing K6: ${error.message}`);
            return;
        }

        if (stderr) {
            console.error(`K6 Error: ${stderr}`);
            return;
        }

        console.log(`K6 Output:\n${stdout}`);
    });
}

// Path to your K6 script
const scriptPath = path.resolve(__dirname, 'main.js');

// Call the function to run the K6 script
runK6Script(scriptPath, 'opt2');  // or 'opt2' or 'default'
