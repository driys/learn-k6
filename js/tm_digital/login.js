import http from 'k6/http';
import { expect } from 'https://jslib.k6.io/k6chaijs/4.3.4.1/index.js';
import { handleResponse, logResponse } from './support/utils.js'; // Utility functions for handling and logging responses
import { postRequest } from './support/httpHelper.js';

export function login(url, email, password) {
    console.log(`---- Login User ----`);
    let path = `${url}/access/login`;
    let body = JSON.stringify({
        "email": email,
        "password": password
    });

    const headers = {
        'Content-Type': 'application/json'
    };
    let response = postRequest(path, body, headers);

    let token = response.json().token;
    let isStatusOK = handleResponse(response);

    if (isStatusOK) {
        console.log(`Login successfully`);
        console.log(`Token: ${token}`);
    } else {
        logResponse(response, `Login Failed`);
    }
    console.log('\n');
    return token;
}