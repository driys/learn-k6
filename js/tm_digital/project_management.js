import http from "k6/http";
import { expect, describe, check } from 'https://jslib.k6.io/k6chaijs/4.3.4.1/index.js';
import { randomIntBetween, randomItem } from "https://jslib.k6.io/k6-utils/1.1.0/index.js";
import { handleResponse, handleGetResponse, logResponse, randomName } from "./support/utils.js";
import { createHeaders, getRequest, postRequest, putRequest } from "./support/httpHelper.js";


// Create New Project as superadmin
export function createNewProject(url, token) {
    console.log(`---- Create New Project ----`);
    let name = randomName('Dummy');
    let path = `${url}/project/add`;
    let body = JSON.stringify({
        "name": name,
        "description": `Deskripsi Project ${name}`,
        "type": "fix bid",
        "start_date": "2023-10-20",
        "end_date": "2024-11-30",
        "platform": [{
            "id_platform": 2,
            "platform": "Website"
        }],
        "guest": [{
            "id_guest": 102
        }],
        "qa": [{
            "id_qa": 48
        }],
        "technology": [{
            "id_technology": `${randomItem([10, 11, 16])}`
        }],
        "testing_application": [{
            "id_testing_application": `${randomItem([7, 8, 15])}`
        }],
        "project_image": [{
            "contentType": "image/jpeg",
            "size": 197158,
            "base64": "test"
        }]
    });
    const headers = createHeaders(token);
    let response = postRequest(path, body, headers);
    
    handleResponse(response);
    console.log(`\n`);
    
    return name;
}

// Get Project by Name as superadmin
export function getProjectByName(url, token, name) {
    console.log(`---- Get Project by Name ${name} ----`);
    let path = `${url}/project/get/name?name=${name}`;
    const headers = createHeaders(token);
    let response = getRequest(path, headers);
    
    let content = response.json().content;
    logResponse(response, content);
    console.log(`\n`);
    
    return content[0].id;
}

//Get Detail Project by ID as superadmin 
export function getDetailProject(url, token, id) {
    console.log(`---- Get Detail Project by ID ${id} ----`);
    let path = `${url}/project/get/${id}`;
    const headers = createHeaders(token);

    let response = getRequest(path, headers);

    handleGetResponse(response);
    console.log(`\n`);
};

// Edit Project as superadmin
export function editProject(url, token, id) {
    console.log(`---- Update Project ----`);
    let name = randomName('Update_Project');
    let path = `${url}/project/update/${id}`;
    let body = JSON.stringify({
        "name": name,
        "status": "on going",
        "description": `Edit Deskripsi Project ${name}`,
        "type": "et",
        "start_date": "2023-10-25",
        "end_date": "2024-12-15",
        "platform": [{
            "id_platform": 2,
            "platform": "Website"
        },
        {
            "id_platform": 3,
            "platform": "Mobile Android"
        }],
        "guest": [{
            "id_guest": 102
        }],
        "qa": [{
            "id_qa": 14
        }],
        "technology": [{
            "id_technology": `${randomItem([10, 11, 16])}`
        }],
        "testing_application": [{
            "id_testing_application": `${randomItem([7, 8, 15])}`
        }],
        "project_image": [{
            "contentType": "image/jpeg",
            "size": 197158,
            "base64": "test"
        }]
    });
    const headers = createHeaders(token);
    let response = putRequest(path, body, headers);
    
    handleResponse(response);
    console.log(`\n`);
}

//Delete Project as superadmin
export function deleteProject(url, token, id) {
    console.log(`---- Delete Project ----`);
    let path = `${url}/project/delete/${id}`;
    
    const headers = createHeaders(token);
    let response = putRequest(path, null, headers);
    
    handleResponse(response);
    console.log(`\n`);
}