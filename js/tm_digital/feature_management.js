import http from "k6/http";
import { expect, describe, check } from 'https://jslib.k6.io/k6chaijs/4.3.4.1/index.js';
import { randomIntBetween, randomItem } from "https://jslib.k6.io/k6-utils/1.1.0/index.js";
import { handleResponse, logResponse, randomName } from "./support/utils.js";
import { createHeaders, getRequest, postRequest, putRequest } from "./support/httpHelper.js";


// Create New Feature as superadmin
export function createNewFeature(url, token) {
    console.log(`---- Create New Feature ----`);
    let name = randomName('Feature Dummy');
    let project_id = 240;
    let path = `${url}/feature/add`;
    let body = JSON.stringify({
        "name": name,
        "description": `Deskripsi ${name}`,
        "created_by": 1,
        "project_id": project_id,
        "qa": [
            {
                "id_qa": `${randomItem([14, 40])}`
            }
        ]
    });
    const headers = createHeaders(token);
    let response = postRequest(path, body, headers);

    handleResponse(response)
    console.log(`\n`);

    return name, project_id;
};

// Method GET namun ada bodynya
// Get All Feature as superadmin
export function getAllFeature(url, token, project_id) {
    console.log(`---- Get All Feature ----`);
    let path = `${url}/feature/all-list?is_deleted=no&order=desc`;
    let body = JSON.stringify({
        "project_id": project_id,
        "name": "",
        "qa": [
            {
                "id_qa": 14
            }
        ],
        "status": null
    });
    const headers = createHeaders(token);

    let response = getRequest(path,body, headers);
    let content = response.json().content;

    logResponse(response, content);
    console.log(`\n`);
    return content[0].id;
};

// Get Detail Feature by ID as superadmin 
export function getDetailFeature(url, token, id) {

    console.log(`---- Get Detail Feature by ID ${id} ----`);
    let path = `${url}/Feature/get/id`;
    const headers = createHeaders(token);

    let response = getRequest(path, headers);

    // let resp = response.json();
    handleResponse(response);
    console.log(`\n`);
};

// Update Feature as superadmin
export function updateFeature(url, token, id) {
    console.log(`---- Update Feature ----`);
    let name = randomName('Edit Feature');
    let path = `${url}/feature/update/${id}`;
    let d = new Date();
    let year = d.getFullYear();
    let body = JSON.stringify({
        "feature": {
            "id": 115,
            "id_specification": 4,
            "name": `${name}`,
            "description": `Deskripsi ${name}`,
            "status": "success",
            "modified_at": year,
            "created_by": 1,
            "modified_by": 1,
            "project_id": project_id,
            "qa": [
                {
                    "id_qa": 14,
                    "qa": "Haris Abdullah"
                }
            ]
        }
    });
    const headers = createHeaders(token);
    let response = putRequest(path, body, headers);
    handleResponse(response);
    console.log(`\n`);
};

//Delete Feature as superadmin
export function deleteFeature(url, token, id) {
    console.log(`---- Delete Feature ----`);
    let path = `${url}/feature/delete/${id}`;
    const headers = createHeaders(token);
    let response = putRequest(path, null, headers);
    handleResponse(response);
    console.log(`\n`);
};