// utils.js
import { check } from 'k6';
import { expect } from 'https://jslib.k6.io/k6chaijs/4.3.4.1/index.js';

export function randomName(prefix) {
    return `${prefix}${Math.floor(Math.random() * 100000)}`;
}

export function randomEmail(prefix) {
    return `${prefix}${Math.floor(Math.random() * 1000)}`;
}

// Used for Get Detail Project
export function handleGetResponse(response) {
    let isStatusOK = check(response, {
        'status is 200': (r) => r.status === 200,
    });

    if (isStatusOK) {
        let content = response.json();
        console.log(`Status Response: ${response.status} OK`);
        console.log(`ID: ${content.id} - Name: ${content.name}`);
        console.log(`Type: ${content.type}`);
        console.log(`Start Date: ${content.start_date} - End Date: ${content.end_date}`)
    } else {
        console.log(`Request failed ${response.status} - ${response.body}`);
    }

    return isStatusOK;
}

export function handleResponse(response) {
    let isStatusOK = check(response, {
        'status is 200': (r) => r.status === 200,
    });

    if (isStatusOK) {
        let content = response.json().message;
        console.log(`Status Response: ${response.status} OK`);
        console.log(`Message: ${content}`);
    } else {
        console.log(`Request failed ${response.status} - ${response.body}`);
    }

    return isStatusOK;
}

export function logResponse(response, content) {
    if (response.status === 200) {
        expect(response.status, 'response status').to.equal(200);
        console.log(`Status Response: ${response.status} OK`);
        if (Array.isArray(content)) {
            content.forEach(item => console.log(`Content: ${item.id} - ${item.name}`));
        } else {
            console.log(`Message: ${content}`);
        }
    } else {
        console.log(`Operation failed ${response.status} - ${response.body}`);
    }
}
