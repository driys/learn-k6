// httpHelpers.js

import http from 'k6/http';

export function createHeaders(token) {
    return {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    };
}

export function postRequest(url, body, headers) {
    return http.post(url, body, { headers: headers });
}

export function getRequest(url, headers) {
    return http.get(url, { headers: headers });
}

export function putRequest(url, body, headers) {
    return http.put(url, body, { headers: headers });
}
