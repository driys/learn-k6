// run command
// k6 run -e TEST_ENV=opt1 main.js // command with if else options
// $env:K6_WEB_DASHBOARD='true'; k6 run main.js // command with web dasboard

export function setup() {

    const BASE_URL = "https://be-tmdigital.stagingapps.net";

    return { url: BASE_URL };
}

// export const options = {
//     vus: 1,          // Number of Virtual Users
//     // duration: '5s',   // Test duration example 10s to use second or 1m to use minute
//     iterations: 1,  // Total number of iterations (this overrides duration)
// };


// Options with if else conditions
const env = __ENV.TEST_ENV || 'default';

export let options;

if (env === 'opt1') {
    options = {
        vus: 1,          // Number of Virtual Users
        // duration: '5s',   // Test duration example 10s to use second or 1m to use minute
        iterations: 1,  // Total number of iterations (this overrides duration)
    };
} else if (env === 'opt2') {
    options = {
        vus: 2,          // Number of Virtual Users
        // duration: '5s',   // Test duration example 10s to use second or 1m to use minute
        iterations: 2,  // Total number of iterations (this overrides duration)
    
    };
} else {
    options = {
        vus: 1,          // Number of Virtual Users
        duration: '5s',   // Test duration example 10s to use second or 1m to use minute
        // iterations: 1,  // Total number of iterations (this overrides duration)
    
    };
}