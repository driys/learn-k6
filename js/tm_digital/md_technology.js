// technology.js
import { createHeaders, postRequest, getRequest, putRequest } from './support/httpHelper.js';
import { randomName, handleResponse, logResponse } from './support/utils.js';

export function createTechnology(url, token) {
    console.log(`---- Create New Technology ----`);

    let name = randomName('Technology');
    const created_by = 'super admin';
    let path = `${url}/technology/add`;
    let body = JSON.stringify({
        "name": name,
        "created_by": created_by
    });
    let headers = createHeaders(token);

    let response = postRequest(path, body, headers);
    handleResponse(response);

    console.log('\n');
    return name;
}

export function getAllTechnology(url, token) {
    console.log(`---- Get All Technology ----`);

    let path = `${url}/technology/all?order=desc`;
    let headers = createHeaders(token);

    let response = getRequest(path, headers);
    let content = response.json().content;

    logResponse(response, content);

    console.log('\n');
}

export function getTechnologyByName(url, token, name) {
    console.log(`---- Get Technology by Name ${name} ----`);

    let path = `${url}/technology/get/name?name=${name}`;
    let headers = createHeaders(token);

    let response = getRequest(path, headers);
    let content = response.json().content;

    logResponse(response, content);

    console.log('\n');
    return content[0].id;
}

export function editTechnology(url, token, id) {
    console.log(`---- Edit Technology with id ${id} ----`);

    let name = randomName('Edit Technology');
    const created_by = 'super admin';
    let path = `${url}/technology/edit/${id}`;
    let body = JSON.stringify({
        "name": name,
        "created_by": created_by
    });
    let headers = createHeaders(token);

    let response = putRequest(path, body, headers);
    handleResponse(response);

    console.log('\n');
}

export function deleteTechnology(url, token, id) {
    console.log(`---- Delete Technology ----`);

    let path = `${url}/technology/delete/${id}`;
    let headers = createHeaders(token);

    let response = putRequest(path, null, headers);
    handleResponse(response);

    console.log('\n');
}
