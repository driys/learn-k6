import http from "k6/http";
import { expect, describe, check } from 'https://jslib.k6.io/k6chaijs/4.3.4.1/index.js';
import { randomIntBetween, randomItem } from "https://jslib.k6.io/k6-utils/1.1.0/index.js";
import { handleResponse, logResponse, randomEmail, randomName } from "./support/utils.js";
import { createHeaders, getRequest, postRequest, putRequest } from "./support/httpHelper.js";

// Create User as superadmin
export function createUser(url, token) {
    console.log(`---- Create User ----`);
    let name = randomName('DummyUser');
    let email = randomEmail('usermail');
    let role = randomItem(["qa", "Guest"]);
    let path = `${url}/usercms/add`;
    let body = JSON.stringify({
        "name": name,
        "username": `${name} ${role}`,
        "email": `${email}@mail.test`,
        "role": role
    });
    console.log
    const headers = createHeaders(token);
    let response = postRequest(path, body, headers);
    handleResponse(response);

    console.log(`\n`);
    return name;
}

//Get All User as superadmin
export function getAllUser(url, token) {
    console.log(`---- Get All User -----`);
    let order = "desc";
    let path = `${url}/usercms/all?order=${order}`;
    const headers = createHeaders(token);
    let response = getRequest(path, headers);
    handleResponse(response);
    console.log(`\n`);
}

//Get User by Name as superadmin
export function getUserByName(url, token, name) {
    console.log(`---- Get User by Name: ${name} ----`);
    let path = `${url}/usercms/get/name/?name=${name}`;

    const headers = createHeaders(token);
    let response = getRequest(path, headers);

    let content = response.json().content;
    logResponse(response, content);

    console.log(`\n`);
    return content[0].id;
}

//Update User as superadmin
export function updateUser(url, token, id, name) {
    console.log(`---- Update User: ${name} ----`);
    name = randomName('EditDummyUser');
    let role = randomItem(["qa", "guest"]);
    let path = `${url}/usercms/edit/${id}`;
    let body = JSON.stringify({
        "name": name,
        "username": `${name} ${role}`,
        "email": `editedmail@mail.test`,
        "role": role
    });
    
    const headers = createHeaders(token);
    let response = putRequest(path, body, headers);
    
    handleResponse(response);
    console.log(`\n`);
}

//Update Status User as superadmin
export function updateUserStatus(url, token, id, name) {
    console.log(`---- Update Status User: ${name} ----`);
    let status = "true";
    let path = `${url}/usercms/edit/status/${id}`;
    let body = JSON.stringify({
        "is_active": status,
    });
    
    const headers = createHeaders(token);
    let response = putRequest(path, body, headers);
    
    handleResponse(response);
    console.log(`\n`);
}

//Delete User as superadmin
export function deleteUser(url, token, id, name) {
    console.log(`---- Delete User: ${name} ----`);
    let path = `${url}/usercms/delete/${id}`;
    const headers = createHeaders(token);
    let response = putRequest(path, null, headers);
    handleResponse(response);
    console.log(`\n`);
}