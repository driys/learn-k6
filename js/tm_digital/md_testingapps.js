import { randomIntBetween, randomItem } from "https://jslib.k6.io/k6-utils/1.1.0/index.js";
import { createHeaders, getRequest, postRequest, putRequest } from "./support/httpHelper.js";
import { randomName, handleResponse, logResponse } from "./support/utils.js";

// Create New Testing Apps as superadmin
export function createTestingApps(url, token) {

    console.log(`---- Create New Testing Apps ----`);
    let name = randomName('Testing_Apps');
    const created_by = 'super admin';
    let path = `${url}/testing-app/add`;
    let body = JSON.stringify({
        "name": name,
        "created_by": created_by
    });
    let headers = createHeaders(token)
    let response = postRequest(path, body, headers)
    handleResponse(response);

    console.log('\n');
    return name;
}

//Get All Testing Apps as superadmin
export function getAllTestingApps(url, token) {

    console.log(`---- Get All Testing Apps ----`);
    let order = "desc";
    let path = `${url}/testing-app/all?order=${order}`;
    let headers = createHeaders(token);

    let response = getRequest(path, headers);
    let content = response.json().content;

    logResponse(response, content);

    console.log(`\n`);
}

//Get Testing Apps by Name as superadmin
export function getTestingAppByName(url, token, name) {
    console.log(`---- Get Testing Apps by Name ${name} ----`);

    let path = `${url}/testing-app/get/name?name=${name}`;
    let headers = createHeaders(token);

    let response = getRequest(path, headers);
    let content = response.json().content;

    logResponse(response, content);

    console.log(`\n`);
    return content[0].id;
}

// Edit Testing Apps as superadmin
export function editTestingApps(url, token, id) {

    console.log(`---- Edit Testing Apps with id ${id} ----`);
    let name = randomItem('Edit Testing Apps');
    const created_by = 'super admin';
    let path = `${url}/testing-app/edit/${id}`;
    let body = JSON.stringify({
        "name": name,
        "created_by": created_by
    });

    let headers = createHeaders(token);
    let response = putRequest(path, body, headers);

    handleResponse(response);
    console.log(`\n`);
}

//Delete Testing Apps as superadmin
export function deleteTestingApps(url, token, id) {

    console.log(`---- Delete Testing Apps ----`);
    let path = `${url}/testing-app/delete/${id}`;
    let headers = createHeaders(token);

    let response = putRequest(path, null, headers);
    handleResponse(response);

    console.log(`\n`);
}