import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
  vus: 10,
  duration: '30s',
};

export default function () {
  // Define the API endpoint
  const url = 'https://reqres.in/api/users';

  // Define the request payload (user data)
  const payload = {
    name: 'John Doe',
    job: 'Software Engineer',
  };

  // Define request headers
  const headers = {
    'Content-Type': 'application/json',
  };

  // Send a POST request to create a user
  const response = http.post(url, JSON.stringify(payload), { headers: headers });

  // Check if the request was successful (status code 201)
  check(response, {
    'Create User Successful': (res) => res.status === 201,
  });

  // Add a short sleep to simulate a realistic user scenario
  sleep(1);
}
