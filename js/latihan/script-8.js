import http from 'k6/http';
import { check, sleep } from 'k6';

const BASE_URL = 'https://reqres.in/api';

export const options = {
  duration: '10s',
  vus: 1,
};

export default function () {
  // Step 1: Create a new user
  const createUserPayload = {
    name: 'John Doe',
    job: 'QA Engineer',
  };

  const createUserResponse = http.post(`${BASE_URL}/users`, JSON.stringify(createUserPayload), {
    headers: { 'Content-Type': 'application/json' },
  });

  // Check that the user was created successfully (status code 201)
  check(createUserResponse, {
    'Create User Status is 201': (r) => r.status === 201,
  });

  // Extract the created user's ID from the response
  const userId = JSON.parse(createUserResponse.body).id;

  // Log the response data of the create user request
  console.log(`Create User Response: ${JSON.stringify(createUserResponse.body)}`);

  // Sleep for a short duration between requests
  sleep(1);

  // Step 2: Get the list of users
  const getUsersResponse = http.get(`${BASE_URL}/users`);

  // Check that the request was successful (status code     200)
  check(getUsersResponse, {
    'Get Users Status is 200': (r) => r.status === 200,
  });

  // Log the response data of the get users request
  console.log(`Get Users Response: ${JSON.stringify(getUsersResponse.body)}`);

  // Sleep for a short duration between requests
  sleep(1);

  // Step 3: Get the details of the created user
  const getUserResponse = http.get(`${BASE_URL}/users/${userId}`);

  // Check that the request was successful (status code 200)
  check(getUserResponse, {
    'Get User Status is 200': (r) => r.status === 200,
  });

  // Log the response data of the get user request
  console.log(`Get User Response: ${JSON.stringify(getUserResponse.body)}`);
}
