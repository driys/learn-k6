import http from 'k6/http';
import { describe, expect } from 'https://jslib.k6.io/k6chaijs/4.3.4.3/index.js';


// export const options = {
//   vus: 1,
//   duration: '5s',
// };

export default function () {
    describe('Testing lur', () => {
        const response = http.get('https://reqres.in/api/user?page=2');
        // sleep(1);
        expect(response.status, 'response status').to.equal(200);
        expect(response).to.have.validJsonBody();
        expect(response.json().data,  'lodos noob').to.be.an('array');
    })
}