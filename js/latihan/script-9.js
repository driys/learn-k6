import http from 'k6/http';
import { sleep, expect, describe, check } from 'https://jslib.k6.io/k6chaijs/4.3.4.1/index.js';
import { randomIntBetween, randomItem } from "https://jslib.k6.io/k6-utils/1.1.0/index.js";

export const options = {
  vus: 100,
  iterations: 100,
};

export default function () {
  describe('Check Page 1', () => {
    const check1 = http.get('https://reqres.in/api/user?page=1');

    // sleep(1);
    expect(check1.status, 'response status').to.equal(200);
    expect(check1.json().data, 'croc list').to.be.an('array');
    console.log('status code:', check1.status);
    console.log('body:', check1.body);

    for (let i = 0; i < 6; i++) {
      const user = check1.json().data[i];
      if (user) {
        console.log(`User ${i + 1} - ID: ${user.id}, Name: ${user.name}, First Year: ${user.year}, Last Color: ${user.color}`);
      } else {
        console.log(`User ${i + 1} not found`);
      }
    }

  });

  describe('Check Page 2', () => {
    const check2 = http.get('https://reqres.in/api/user?page=2');
    // sleep(1);
    expect(check2.status, 'response status').to.equal(200);
    expect(check2.json().data, 'croc list').to.be.an('array');
    console.log('status code:', check2.status);
    console.log('body:', check2.body);

    for (let i = 0; i < 6; i++) {
      const user = check2.json().data[i];
      if (user) {
        console.log(`User ${i + 1} - ID: ${user.id}, Name: ${user.name}, First Year: ${user.year}, Last Color: ${user.color}`);
      } else {
        console.log(`User ${i + 1} not found`);
      }
    }

  });

  describe('Create User', () => {
    const apiUrl = 'https://reqres.in/api/users';
    const userData = {
      name: `User ${randomIntBetween(1, 100)}`,
      job: randomItem(["QA Manual", "QA Engineer", "Developer"]),
    };
    const headers = {
      'Content-Type': 'application/json',
    };

    // Send a POST request to create a user
    let response = http.post(apiUrl, JSON.stringify(userData), { headers: headers });

    // Log the response details
    if (!response) {
      console.error('Error: No response received.');
    } else {
      console.log(`Status Respons: ${response.status}`);
      console.log(`Body Respons: ${response.body}`);
    }

    // Check if the request was successful (status code 201)
    if (response.status === 201) {
      expect(response.status, 'response status').to.equal(201);
      // check(response, {
      //   'Status Pembuatan Pengguna adalah 201': (res) => res.status === 201,
      // });
    } else {
      console.error(`Unexpected status code: ${response.status}`);
    }
    // sleep(2);
  });
}