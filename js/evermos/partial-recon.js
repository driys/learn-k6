import http from "k6/http";
import { group, check, sleep } from "k6";
import { open } from 'k6';

const BASE_URL = "http://logistic-data-processing.dev.internal";
// Sleep duration between successive requests.
// You might want to edit the value of this variable or remove calls to the sleep function on the script.
const SLEEP_DURATION = 0.1;
// Global variables should be initialized.
let updatedBy = "ferischa@evermos.com";
let xUserid = "ferischa@evermos.com";

let partialReconCSV;
try {
  partialReconCSV = open('/Users/user/Documents/swagger-to-k6/logistic-data-processing.dev.internal/partial-recon.csv', 'b');
} catch (e) {
  console.error('Error opening file:', e.message);
}

export default function () {
  group("/v1/recon-partial/download", () => {
    let pageNumber = 'TODO_EDIT_THE_PAGENUMBER'; // specify value as there is no example value for this parameter in OpenAPI spec
    let endDate = 'TODO_EDIT_THE_ENDDATE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let completedEndDate = 'TODO_EDIT_THE_COMPLETEDENDDATE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let isTotalCodNotMatch = 'TODO_EDIT_THE_ISTOTALCODNOTMATCH'; // specify value as there is no example value for this parameter in OpenAPI spec
    let sbu = 'TODO_EDIT_THE_SBU'; // specify value as there is no example value for this parameter in OpenAPI spec
    let orderStatus = 'TODO_EDIT_THE_ORDERSTATUS'; // specify value as there is no example value for this parameter in OpenAPI spec
    let pageSize = 'TODO_EDIT_THE_PAGESIZE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let remark = 'TODO_EDIT_THE_REMARK'; // specify value as there is no example value for this parameter in OpenAPI spec
    let isAnomaly = 'TODO_EDIT_THE_ISANOMALY'; // specify value as there is no example value for this parameter in OpenAPI spec
    let awbNumber = 'TODO_EDIT_THE_AWBNUMBER'; // specify value as there is no example value for this parameter in OpenAPI spec
    let anomalyStatus = 'TODO_EDIT_THE_ANOMALYSTATUS'; // specify value as there is no example value for this parameter in OpenAPI spec
    let paymentEndDate = 'TODO_EDIT_THE_PAYMENTENDDATE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let filename = 'TODO_EDIT_THE_FILENAME'; // specify value as there is no example value for this parameter in OpenAPI spec
    let doubleInvoicingFilename = 'TODO_EDIT_THE_DOUBLEINVOICINGFILENAME'; // specify value as there is no example value for this parameter in OpenAPI spec
    let isCodFeeNotMatch = 'TODO_EDIT_THE_ISCODFEENOTMATCH'; // specify value as there is no example value for this parameter in OpenAPI spec
    let logisticId = 'TODO_EDIT_THE_LOGISTICID'; // specify value as there is no example value for this parameter in OpenAPI spec
    let isAwbNumberNotFound = 'TODO_EDIT_THE_ISAWBNUMBERNOTFOUND'; // specify value as there is no example value for this parameter in OpenAPI spec
    let completedStartDate = 'TODO_EDIT_THE_COMPLETEDSTARTDATE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let startDate = 'TODO_EDIT_THE_STARTDATE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let paymentStartDate = 'TODO_EDIT_THE_PAYMENTSTARTDATE'; // specify value as there is no example value for this parameter in OpenAPI spec

    // Request No. 1 Download Partial Recon with StartDate and endDate: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?startDate=${'2024-05-01'}&endDate=${'2024-05-01'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 2 Download Partial Recon with completedStartDate and completedEndDate: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?completedStartDate=${'2024-01-01'}&completedEndDate=${'2024-05-01'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 3 Download Partial Recon with completedStartDate and completedEndDate: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?paymentStartDate=${'2024-04-01'}&paymentEndDate=${'2024-05-31'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 4 Download Partial Recon with with StartDate, endDate and remark: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?startDate=${'2024-05-01'}&endDate=${'2024-05-01'}&remark=${'Settled'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 5 Download Partial Recon with with StartDate, endDate, logisticId and orderStatus: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?startDate=${'2024-05-01'}&endDate=${'2024-05-01'}&logisticId=${'1'}&orderStatus=${'COMPLETED,RETURN'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 6 Download Partial Recon with with StartDate, endDate and isTotalCodNotMatch is TRUE: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&isTotalCodNotMatch=${'TRUE'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 7 Download Partial Recon with with StartDate, endDate and isTotalCodNotMatch is FALSE: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&isTotalCodNotMatch=${'FALSE'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 8 Download Partial Recon with with StartDate, endDate and isTotalCodNotMatch is FALSE: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&isTotalCodNotMatch=${'FALSE'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 9 Download Partial Recon with with StartDate, endDate and isCodFeeNotMatch is TRUE: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&isCodFeeNotMatch=${'TRUE'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 10 Download Partial Recon with with StartDate, endDate and isCodFeeNotMatch is FALSE: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&isCodFeeNotMatch=${'TRUE'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 11 Download Partial Recon with with StartDate, endDate and sbu is Everpro: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&sbu=${'Everpro'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 12 Download Partial Recon with with StartDate, endDate and sbu is Evermos: 
    {
      let url = BASE_URL + `/v1/recon-partial/download?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&sbu=${'Evermos'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    }
  });

  group("/v1/recon-partial", () => {
    let pageNumber = 'TODO_EDIT_THE_PAGENUMBER'; // specify value as there is no example value for this parameter in OpenAPI spec
    let endDate = 'TODO_EDIT_THE_ENDDATE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let completedEndDate = 'TODO_EDIT_THE_COMPLETEDENDDATE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let isTotalCodNotMatch = 'TODO_EDIT_THE_ISTOTALCODNOTMATCH'; // specify value as there is no example value for this parameter in OpenAPI spec
    let sbu = 'TODO_EDIT_THE_SBU'; // specify value as there is no example value for this parameter in OpenAPI spec
    let orderStatus = 'TODO_EDIT_THE_ORDERSTATUS'; // specify value as there is no example value for this parameter in OpenAPI spec
    let pageSize = 'TODO_EDIT_THE_PAGESIZE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let remark = 'TODO_EDIT_THE_REMARK'; // specify value as there is no example value for this parameter in OpenAPI spec
    let isAnomaly = 'TODO_EDIT_THE_ISANOMALY'; // specify value as there is no example value for this parameter in OpenAPI spec
    let awbNumber = 'TODO_EDIT_THE_AWBNUMBER'; // specify value as there is no example value for this parameter in OpenAPI spec
    let anomalyStatus = 'TODO_EDIT_THE_ANOMALYSTATUS'; // specify value as there is no example value for this parameter in OpenAPI spec
    let paymentEndDate = 'TODO_EDIT_THE_PAYMENTENDDATE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let filename = 'TODO_EDIT_THE_FILENAME'; // specify value as there is no example value for this parameter in OpenAPI spec
    let doubleInvoicingFilename = 'TODO_EDIT_THE_DOUBLEINVOICINGFILENAME'; // specify value as there is no example value for this parameter in OpenAPI spec
    let isCodFeeNotMatch = 'TODO_EDIT_THE_ISCODFEENOTMATCH'; // specify value as there is no example value for this parameter in OpenAPI spec
    let logisticId = 'TODO_EDIT_THE_LOGISTICID'; // specify value as there is no example value for this parameter in OpenAPI spec
    let isAwbNumberNotFound = 'TODO_EDIT_THE_ISAWBNUMBERNOTFOUND'; // specify value as there is no example value for this parameter in OpenAPI spec
    let completedStartDate = 'TODO_EDIT_THE_COMPLETEDSTARTDATE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let startDate = 'TODO_EDIT_THE_STARTDATE'; // specify value as there is no example value for this parameter in OpenAPI spec
    let paymentStartDate = 'TODO_EDIT_THE_PAYMENTSTARTDATE'; // specify value as there is no example value for this parameter in OpenAPI spec

    // Request No. 1: Filter Partial Recon with StartDate and endDate: 
    {
      let url = BASE_URL + `/v1/recon-partial?startDate=${'2024-05-01'}&endDate=${'2024-05-01'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 2: Filter Partial Recon with completedStartDate and completedEndDate: 
    {
      let url = BASE_URL + `/v1/recon-partial?completedStartDate=${'2024-01-01'}&completedEndDate=${'2024-05-01'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 3: Filter Partial Recon with paymentStartDate and paymentEndDate: 
    {
      let url = BASE_URL + `/v1/recon-partial?paymentStartDate=${'2024-04-01'}&paymentEndDate=${'2024-05-31'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 4: Filter Partial Recon with Recon with with StartDate, endDate and remark: 
    {
      let url = BASE_URL + `/v1/recon-partial?startDate=${'2024-05-01'}&endDate=${'2024-05-01'}&remark=${'Settled'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 5 Filter Partial Recon with with StartDate, endDate, logisticId and orderStatus: 
    {
      let url = BASE_URL + `/v1/recon-partial?startDate=${'2024-05-01'}&endDate=${'2024-05-01'}&logisticId=${'1'}&orderStatus=${'COMPLETED,RETURN'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 6 Filter Partial Recon with with StartDate, endDate and isTotalCodNotMatch is TRUE: 
    {
      let url = BASE_URL + `/v1/recon-partial?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&isTotalCodNotMatch=${'TRUE'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 7 Filter Partial Recon with with StartDate, endDate and isTotalCodNotMatch is FALSE: 
    {
      let url = BASE_URL + `/v1/recon-partial?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&isTotalCodNotMatch=${'FALSE'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 8 Filter Partial Recon with with StartDate, endDate and isTotalCodNotMatch is FALSE: 
    {
      let url = BASE_URL + `/v1/recon-partial?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&isTotalCodNotMatch=${'FALSE'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 9 Filter Partial Recon with with StartDate, endDate and isCodFeeNotMatch is TRUE: 
    {
      let url = BASE_URL + `/v1/recon-partial?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&isCodFeeNotMatch=${'TRUE'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 10 Filter Partial Recon with with StartDate, endDate and isCodFeeNotMatch is FALSE: 
    {
      let url = BASE_URL + `/v1/recon-partial?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&isCodFeeNotMatch=${'TRUE'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 11 Filter Partial Recon with with StartDate, endDate and sbu is Everpro: 
    {
      let url = BASE_URL + `/v1/recon-partial?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&sbu=${'Everpro'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    };
    // Request No. 12 Filter Partial Recon with with StartDate, endDate and sbu is Evermos: 
    {
      let url = BASE_URL + `/v1/recon-partial?startDate=${'2023-09-01'}&endDate=${'2024-05-01'}&sbu=${'Evermos'}`;
      let request = http.get(url);

      check(request, {
        "OK": (r) => r.status === 200
      });
    }
  });
  if (!partialReconCSV) {
    console.error('File not found or unable to open.');
  } else {
    group("/v1/recon-partial/import", () => {
      // Define the URL for the request
      let url = BASE_URL + `/v1/recon-partial/import`;

      // Prepare the body with the file
      let body = { file: http.file(partialReconCSV, "partial_recon.csv") };

      // Define the headers
      let params = {
        headers: {
          "Content-Type": "multipart/form-data",
          "x-userid": xUserid,
          "Accept": "application/json"
        }
      };

      // Send the POST request
      let request = http.post(url, body, params);

      // Check the response
      check(request, {
        "status is 200": (r) => r.status === 200
      });

      // Log the response for debugging
      if (request.status !== 200) {
        console.error(`Request failed with status ${request.status}`);
        console.error(`Response body: ${request.body}`);
      }
    });
  }
  group("/v1/recon-partial/revision/import", () => {

    // Request No. 1: 
    {
      let url = BASE_URL + `/v1/recon-partial/revision/import`;
      // TODO: edit the parameters of the request body.
      let body = { "file": http.file(open("/path/to/file.bin", "b"), "test.bin") };
      let params = { headers: { "Content-Type": "multipart/form-data", "x-userid": `${xUserid}`, "Accept": "application/json" } };
      let request = http.post(url, JSON.stringify(body), params);

      check(request, {
        "OK": (r) => r.status === 200
      });
    }
  });
}    